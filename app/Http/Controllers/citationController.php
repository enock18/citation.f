<?php

namespace App\Http\Controllers;

use App\Models\Auteur;
use App\Models\Citation;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\VarDumper;

class citationController extends Controller
{
    //

    public function latestCitation(){
        
        $latestCitation = Citation::with('auteur')->orderBy('created_at', 'desc')->take(1)->get();
        return view('accueil')->with('news',$latestCitation);
    }
    public function showCitations()
    {
        $citations= Citation::with('auteur')->get();
        return view('citation')->with('citations', $citations); 
    }

    public function citationsbackoffice(){
        $citations= Citation::all();
        return view('backofficitation')->with('citations', $citations);
    }
    public function citationStore(){
        $auteurs=Auteur::all();
        return view('addcitation')->with('auteurs', $auteurs); 
    }
    public function citationPost(Request $request){
       
        $post=new Citation();
        $post->citation = $request->citation;
        $post->auteur_id = $request->auteur;
        $post->save();
        return redirect()->route('citation.store');

    }
}
