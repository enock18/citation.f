<?php

namespace App\Http\Controllers;

use App\Models\Auteur;
use Illuminate\Support\Facades\DB;

class auteurController extends Controller
{
    //
    public function showAuteurs () {
        $auteurs=Auteur::all();
        return view('auteur')->with('auteurs', $auteurs); 
    }
    public function auteursbackoffice () {
        $auteurs=Auteur::all();
        return view('backofficeauteur')->with('auteurs', $auteurs);
    }
    public function addAuteurPage(){
        
        return view('addauteur');
    }
    public function showAuteurCitation($id,$auteurname){
        $citations = DB::table('citation')->where('auteur_id', $id)->get();
        return view('auteurcitation',compact('auteurname','citations'));
    }
}
