<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Citation extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'citation';
    public function auteur()
    {
        return $this->belongsTo(Auteur::class);
    }
}
