<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auteur extends Model
{
    use HasFactory;
    protected $table = 'auteur';
    public function citation()
    {
        return $this->hasMany(Citation::class);
    }
}
