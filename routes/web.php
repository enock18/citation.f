<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\auteurController;
use App\Http\Controllers\citationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [citationController::class, 'latestCitation']);

Route::get('/auteur', [auteurController::class, 'showAuteurs']);
Route::get('/auteurback', [auteurController::class, 'auteursbackoffice']);
Route::get('/citation', [citationController::class, 'showCitations']);
Route::get('/citationback', [citationController::class, 'citationsbackoffice']);
// Ajouter les citations et auteurs
Route::get('/addcitation',[citationController::class, 'citationStore'])->name("citation.store");
Route::post('/addcitation',[citationController::class, 'citationPost'])->name("citation.post");
// Ajouter  les citations et auteurs
Route::get('/addauteur',[auteurController::class, 'removeCitation']);
// voir les citations d'un auteur
Route::get('/citations/{id}/{auteurname}',[auteurController::class, 'showAuteurCitation']);
