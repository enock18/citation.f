
@extends('layouts.base')

@section('title', 'citations auteur')

@section('content')
<img src="/images/bg.jpg" alt="background" width="100%" >


   
    
 <h1 style="font-size:2em;margin-bottom:100px">Les citations de {{$auteurname}}</h1>
 <div class="container-citation">
            <div>
                @foreach ($citations as $citation)
                    <div class="citation">
                        <p>{{$citation->citation }}</p>
                      
                    </div>       
                @endforeach
            </div>

            <div class="man-container">
                <div>
                    <img class="man-image"src="/images/imageshome.jpg" alt="images" width="60">
                </div>
                    <h4>Eric Dupont</h4>
                    <button class="button-follow">Follow</button>
                    <p>Entrepreneur à plein temps</p>
                    <p>Location:</p>
                    <p>France</p>  
            </div>

</div>


@endsection


