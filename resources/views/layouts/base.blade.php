<html>
    <head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/app.css" class="re">

        <title>@yield('title')</title>

    </head>
    <body>
        
        
 
        
            <header>
                <div class="menu-desktop">
                    <div class="logo">
                        <img src="/images/logo.png" alt="logo" width="40px"/>
                    </div>
                    <ul class="list-ul">
                           
                        <li> <a href="{{ asset('/') }}">Accueil</a></li>
                        <li><a href="{{ asset('/citation') }}">Citations</a> </li>
                        <li><a href="{{ asset('/auteur') }}">Auteurs</a></li>
                       {{-- <input type="search" name="" id="" placeholder="Search"s> --}}
                    </ul>
                </div>
                
        
                <div id="sidebar">
                    <div class="toggle-button" id="toggle-button">
                         
                        <span></span>
                        <span></span>
                        <span></span>
            
            
                    </div>
                    
                    <ul class="link-mobile">
                        <li><a href="">Accueil</a></li>
                        <li><a href="">Citations</a> </li>
                        <li><a href="">Auteurs</a> </li>
                        
                    </ul>
            
                </div>

            </header>

            @yield('content')
            <footer>
                <p>&copy copyright {{ date("Y") }} citation.fr</p>
            </footer>
       

       
    </body>
    <script src="/js/script.js"></script> 
</html>