@extends('layouts.base')

@section('title', 'Accueil')

@section('content')
{{-- <img src="/images/10.jpg" alt="background" width="100%" height="100%" > --}}


<div class="poem">
	<figure>
		<p>Turn back </p>

		<figcaption>
			@foreach ( $news as $new )	
			{{ $new->citation }}
			
			<div class="author"> <a href="/citations/{{ $new->auteur_id}}/{{$new->auteur->name}}"> ― {{ $new->auteur->name }}</a> </div>
			@endforeach
		</figcaption>
	</figure>

</div>

<h2>Hover over this beautiful image</h2>
  
@endsection

