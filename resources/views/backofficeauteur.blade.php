@extends('layouts.backoffice')

@section('title', 'backoficeauteur')

@section('content')
{{-- <img src="/images/10.jpg" alt="background" width="100%" height="100%" > --}}
<div class="container-bakoffice">
    <h1>Les auteurs</h1>

    <button class="btn-ajouter">Ajouter un auteur</button>
    <table>
        <thead>
            <tr>
                <th>
                    id
                </th>
                <th>
                   Auteurs
                </th>
                <th>
                   Action
                </th>
            </tr>
            
    
        </thead>
        <tbody>
            @foreach ($auteurs as $auteur )
                
            
            <tr>
                <td>
                 {{ $auteur->id}}
                </td>
                <td>
                    {{ $auteur ->name}}
                </td>
                <td>
                   <button class="btns-sup">Modifier</button>
                   <button class="btns-sup">Supprimer</button>
                 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
</div>


  
@endsection
