@extends('layouts.backoffice')

@section('title', 'backoficeauteur')

@section('content')
{{-- <img src="/images/10.jpg" alt="background" width="100%" height="100%" > --}}
<div>
    <h1>Les citations</h1>
    <button class="btn-ajouter"><a href="{{ asset('addcitation') }}">Ajouter une citation</a> </button>
    <table>
        <thead>
            <tr>
                <th>
                    id
                </th>
                <th>
                    name
                </th>
                <th>
                    Action
                </th>
            </tr>
            
    
        </thead>
        <tbody>
            @foreach ($citations as $citation)
                
           
            <tr>
                <td>
                   {{ $citation->id }}
                </td>
                <td>
                    {{ $citation->citation }} 
                </td>
                <td>
                    <button class="btns-sup">Modifier</button>
                    <button class="btns-sup">Supprimer</button>
                  
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>




  
@endsection