@extends('layouts.base')

@section('title', 'Accueil')

@section('content')
<img src="/images/bg.jpg" alt="background" width="100%" >

<div class="container-citation">
            <div>
                @foreach ($citations as $citation)
                    <div class="citation">
                        <p>{{ $citation->citation }}</p>
                        <h4><a href="/citations/{{ $citation->auteur_id}}/{{ $citation->auteur->name }}">{{ $citation->auteur->name }}</a></h4>
                    </div>       
                @endforeach
            </div>

            <div class="man-container">
                <div>
                    <img class="man-image"src="/images/imageshome.jpg" alt="images" width="60">
                </div>
                    <h4>Eric Dupont</h4>
                    <button class="button-follow">Follow</button>
                    <p>Entrepreneur à plein temps</p>
                    <p>Location:</p>
                    <p>France</p>  
            </div>

</div>


@endsection
