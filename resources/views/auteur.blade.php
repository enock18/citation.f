@extends('layouts.base')

@section('title', 'auteur')

@section('content')
<img src="/images/1.jpg" alt="background" width="100%" height="100%" >
<h1>Liste des auteurs</h1>
<table>
    <thead>
        <tr>
            <th>
                id
            </th>
            <th>
                Auteur
            </th>
            
        </tr>
        

    </thead>
    <tbody>
        @foreach ($auteurs as $auteur)
            
      
        <tr>
            <td>
              {{$auteur->id }}
            </td>
            <td>
                {{$auteur->name }}
            </td>
            
        </tr>
        @endforeach
    </tbody>
    
</table>

@endsection
