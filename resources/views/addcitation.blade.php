@extends('layouts.backoffice')

@section('title', 'backoficeauteur')

@section('content')
{{-- <img src="/images/10.jpg" alt="background" width="100%" height="100%" > --}}
<div>
    <form method="POST" action="{{ route('citation.store') }}">
        @csrf
        {{-- <input type="text" name="auteur" placeholder="auteur"> --}}
        
            
        
        <select name="auteur">
            @foreach ($auteurs as $auteur) 
            <option value="{{ $auteur->id }}">{{ $auteur->name }}</option>
            @endforeach
        </select>
        
        <input type="text" name="citation" placeholder="citation">
        
        <input type="submit" >
    </form>
    
</div>




  
@endsection